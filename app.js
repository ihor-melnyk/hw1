const express = require('express');
const morgan = require('morgan');
const fs = require('fs').promises;
const path = require('path');

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.post('/api/files',(req,res)=>{
  const password = req.query.password;
  const filename = req.body.filename;
  const content = req.body.content;
  const extensions = ['.log','.txt','.json','.yaml','.xml','.js'];
  if(filename && content) {
    if(extensions.includes(path.extname(filename))) {
      (async () => {
        try {
          const raw = await fs.readFile('./files.json', 'utf-8');
          const obj = JSON.parse(raw);
          obj.files.push({name:filename,password:req.query.password || null});
          await fs.writeFile(`./files.json`,JSON.stringify(obj,null,2));
          await fs.writeFile(`./files/${filename}`,content);
          res.status(200).json({message: "File created successfully"});
        } catch(err) {
          console.error(err);
          res.status(500).json({message: "Server error"});
        }
      })();
    } else {
      res.status(400).json({message: "Unknown file extension"});
    }
  } else if(filename) {
    res.status(400).json({message: "Please specify 'content' parameter"});
  } else if(content) {
    res.status(400).json({message: "Please specify 'filename' parameter"});
  } else {
    res.status(400).json({message: "Please specify 'filename' and 'content' parameters"});
  }
});

app.get('/api/files',(req,res)=>{
  (async () => {
    try {
      const data = await fs.readdir('./files');
      res.status(200).json({message: "Success", files: data});
    } catch (err) {
      console.error(err);
      res.status(500).json({message: "Server error"});
    }
  })();
});

app.get('/api/files/:filename',(req,res)=>{
  const filename = req.params.filename;
  const password = req.query.password;
  const filepath = `./files/${filename}`;
  (async () => {
    try {
      const raw = await fs.readFile('./files.json', 'utf-8');
      const obj = JSON.parse(raw);
      const file = obj.files.find(el=>el.name===filename)
      if(file) {
        if(file.password===password || !file.password) {
          const time = await fs.stat(filepath);
          const data = await fs.readFile(filepath, 'utf-8');
          res.status(200).json({message: "Success", filename,
            content: data, extension:path.extname(filename).slice(1),
            uploadedDate: time.birthtime});
        } else {
          res.status(400).json({message: `Incorrect password`});
        }
      } else {
        res.status(400).json({message: `No file with '${filename}' filename found`});
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({message: "Server error"});
    }
  })();
});

app.delete('/api/files/:filename',(req,res)=>{
  const filename = req.params.filename;
  const filepath = `./files/${filename}`;
  const password = req.query.password;
  (async () => {
    try {
      const raw = await fs.readFile('./files.json', 'utf-8');
      const obj = JSON.parse(raw);
      const file = obj.files.find(el=>el.name===filename)
      if(file) {
        if(file.password===password || !file.password) {
          await fs.unlink(filepath);
          await fs.writeFile(`./files.json`,JSON.stringify({files:obj.files.filter((item) => item.name !== filename)},null,2));
          res.status(200).json({message: "File deleted successfully"});
        } else {
          res.status(400).json({message: `Incorrect password`});
        }
      } else {
        res.status(400).json({message: `No file with '${filename}' filename found`});
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({message: "Server error"});
    }
  })();
});

app.patch('/api/files/:filename',(req,res)=>{
  const filename = req.params.filename;
  const filepath = `./files/${filename}`;
  const password = req.query.password;
  const content = req.body.content;
  (async () => {
    try {
      const raw = await fs.readFile('./files.json', 'utf-8');
      const obj = JSON.parse(raw);
      const file = obj.files.find(el=>el.name===filename)
      if(file) {
        if(file.password===password || !file.password) {
          await fs.appendFile(filepath,content);
          res.status(200).json({message: "File modified successfully"});
        } else {
          res.status(400).json({message: `Incorrect password`});
        }
      } else {
        res.status(400).json({message: `No file with '${filename}' filename found`});
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({message: "Server error"});
    }
  })();
});

app.listen(8080);